/**
 * SPDX-PackageName: kwaeri/progress
 * SPDX-PackageVersion: 0.6.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


'use strict'

// INCLUDES
import * as assert from 'assert';
import { Console } from '@kwaeri/console';
import { Progress } from '../src/progress.mjs';


// DEFINES
const progress= new Progress({ spinner: true, spinAnim: "dots", percentage: false });
const _c  = new Console( { color: false, background: false, decor: [] } );

// SANITY CHECK - Makes sure our tests are working proerly
describe(
    'PREREQUISITE',
    () => {
        describe(
            'Sanity Test(s)',
            () => {

                it(
                    'Should return true.',
                    () => {
                        assert.equal( [1,2,3,4].indexOf(4), 3 );
                    }
                );

            }
        );
    }
);


describe(
    'Progress Functionality Test Suite',
    () => {

        describe(
            'sample()',
            () => {
                it(
                    'Should return true (while having shown a progress bar).',
                    async () => {
                        // Increase the default timeout so we can play with timeouts ourselves - which would normally
                        // far exceed the allowed 2000ms allotted to tests:
                        //
                        // this.timeout( 2000 );
                        //
                        // Which didn't work, modifying the test script in package.json did however (add --timeout 2000 )
                        //
                        const routine = async () => {
                            progress.init();

                            return new Promise<any>
                            (
                                ( resolve, reject ) =>
                                {
                                    let firstQuarter = () => { progress.update( 25 ); progress.log( `Added 25%  progress.` ).notify( "Adding another 25% progress.." ); setTimeout( firstishQuarter, 1000 ); },
                                        firstishQuarter = () => { progress.notify( _c.color( "red" ).buffer( "Still working on adding another 25% progress..." ).dump() ); setTimeout( secondQuarter, 1000 ); },
                                        secondQuarter = () => { progress.log( `Added another 25% progress.` ).updateAndNotify( 50, "Adding, again, another 25% progress..." ); setTimeout( thirdQuarter, 500 ); },
                                        thirdQuarter = () => { progress.log( `Added, again, another 25% progress.`, 1 ).updateAndNotify( 75, "Adding yet another 25% progress..." ); setTimeout( fourthQuarter, 2000 ); },
                                        fourthQuarter = () => { progress.log( `Added yet another 25% progress.`, 2 ).updateAndNotify( 100, "Finished" ); progress.notify(); setTimeout( finish, 1000 ); },
                                        finish = () => { progress.log( `Completing test...`, 3 ).stop(); resolve( true ); };

                                    firstQuarter();
                                }
                            );
                        };

                        const result = await routine();

                        assert.equal( result, true );
                    }
                );
            }
        );

    }
);